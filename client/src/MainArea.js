import React, { Component } from "react";

class MainArea extends React.Component {
    constructor(props) {
        super(props);
        this.state = { items: [], title: '', appointDate: '', text: '' };
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    render() {
        return (
            <div>
                <h3>Main Page</h3>
                <p>Added appointments:</p>
                <Appointment items={this.state.items} />
                <form onSubmit={this.handleSubmit}>
                    <label htmlFor="new">
                        New appointment:
            </label>
                    <input
                        id="new-title"
                        onChange={this.handleChange}
                        value={this.state.title}
                    />
                    <input
                        id="new-date"
                        onChange={this.handleChange}
                        value={this.state.appointDate}
                    />
                    <input
                        id="new-desc"
                        onChange={this.handleChange}
                        value={this.state.text}
                    />
                    <button>
                        Add #{this.state.items.length + 1}
                    </button>
                </form>
            </div>
        );
    }

    handleChange(e) {
        this.setState({ text: e.target.value });
    }

    handleSubmit(e) {
        e.preventDefault();
        if (!this.state.text.length) {
            return;
        }
        const newItem = {
            title: this.state.title,
            appointDate: this.state.appointDate,
            text: this.state.text,
            id: Date.now()
        };
        this.setState(state => ({
            items: state.items.concat(newItem),
            title: '',
            appointDate: '',
            text: ''
        }));
    }
}

class Appointment extends React.Component {
    render() {
        return (
            <ol>
                {this.props.items.map(item => (
                    <li key={item.id}>{item.title} - {item.appointDate}. Description: {item.text}</li>
                ))}
            </ol>
        );
    }
}

export default MainArea;