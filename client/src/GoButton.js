import React, { Component } from "react";
import { BrowserRouter, Route, Link } from 'react-router-dom';

export class GoButton extends React.Component {

    render() {
        return (
            <BrowserRouter>
                <Link to={"/main"}>
                    <button type="button">Go!</button>
                </Link>
            </BrowserRouter>
        );
    }

}

export default GoButton;