import React, { Component } from "react";
import "./App.css";
import MainArea from "./MainArea.js";
import "bootstrap/dist/css/bootstrap.css";
import { BrowserRouter, Route, Link, Switch } from 'react-router-dom';
import GoButton from "./GoButton";





class App extends Component {
  state = {
    response: "",
    post: "",
    responseToPost: ""
  };

  constructor(props) {
    super(props);
    this.state = { items: [], title: '' };
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }



  componentDidMount() {
    this.callApi()
      .then(res => this.setState({ response: res.express }))
      .catch(err => console.log(err));
  }
  callApi = async () => {
    const response = await fetch("/api/hello");
    const body = await response.json();
    if (response.status !== 200) throw Error(body.message);
    return body;
  };

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <h2 className="title-main">Appointment Manager</h2>
          <p className="label-main">Username:</p>
          <input className="input-main" />
          <p className="label-main">Password:</p>
          <input className="input-main" />
          <GoButton></GoButton>
          <BrowserRouter>
            <Route exact path={"main"} component={MainArea} />
          </BrowserRouter>

          <p>{this.state.response}</p>

          <div>
            <h3>Main Page</h3>
            <p>Added appointments:</p>
            <Appointment items={this.state.items} />
            <form onSubmit={this.handleSubmit}>
              <label htmlFor="new">
                New appointment:
            </label>
              <input
                name="input1"
                onChange={this.handleChange}
                value={this.state.title}
              />
              <button>
                Add
              </button>
            </form>
          </div>

        </header>
      </div>
    );
  }

  handleChange(e) {
    this.setState({ title: e.target.value });
  }

  handleSubmit(e) {
    e.preventDefault();
    if (!this.state.title.length) {
      return;
    }
    const newItem = {
      title: this.state.title,
      id: Date.now()
    };
    this.setState(state => ({
      items: state.items.concat(newItem),
      title: ''
    }));
  }
}

class Appointment extends React.Component {
  render() {
    return (
      <ol>
        {this.props.items.map(item => (
          <li key={item.id}>{item.title} - {new Date().toLocaleString()} - Description: None</li>
        ))}
      </ol>
    );
  }
}


export default App;
